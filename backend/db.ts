import mongoose from 'mongoose';
import dotenv from 'dotenv';
dotenv.config();
const DATABASE_URL = process.env.DATABASE_URL || 'mongodb://localhost:27017/';

mongoose.connect(DATABASE_URL)
  .then(() => {
    console.log('Database connected');
  })
  .catch((err) => {
    console.error('Database connection error:', err);
  });

const userSchema = new mongoose.Schema({
  name: String,
  username: String,
  password: String,
  email: String,
  phoneNumber: String,
  books: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Books' }],
});

const User = mongoose.model('User', userSchema);

const adminSchema = new mongoose.Schema({
  name: String,
  username: String,
  password: String,
  email: String,
  phoneNumber: String,
});

const Admin = mongoose.model('Admin', adminSchema);

const bookSchema = new mongoose.Schema({
  name: { type: String, minLength: 1 },
  author: { type: String, minLength: 1 },
  available: { type: Boolean, default: false },
});

const Books = mongoose.model('Books', bookSchema);

const transactionSchema = new mongoose.Schema({
  bookId: { type: mongoose.Schema.Types.ObjectId, ref: 'Books' },
  userId: { type: mongoose.Schema.Types.ObjectId, ref: 'User' },
  dueDate: Date,
  status: String,
});

const Transactions = mongoose.model('Transactions', transactionSchema);

export { User, Admin, Books, Transactions };
