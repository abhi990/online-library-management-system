import express from 'express';
import zod from 'zod';
import jwt from 'jsonwebtoken';
import { User, Admin, Books, Transactions } from '../db';
import adminAuthMiddleware from '../middleware/adminAuthMiddleware';

const router = express.Router();
const JWT_SECRET = process.env.JWT_SECRET || 'Codefeast';

const signupBody = zod.object({
    username : zod.string(),
    name : zod.string(),
    email : zod.string().email(),
    password : zod.string(),
    phoneNumber : zod.string()
})

const loginBody= zod.object({
    username : zod.string(),
    password : zod.string()
})

//Route for Admin Login, returns a JWT token
router.post("/login", async (req, res) => {
    const username = req.body.username;
    const password = req.body.password;

    const success = loginBody.safeParse(req.body)
    if(!(success.success)){
        return res.status(411).json({
            "message" : "Invalid Inputs"
        })
    }

    const user= await Admin.findOne({
        username : username,
        password : password
    })

    if(!user){
        return res.status(411).json({
            "message" : "Invalid Credentials"
        })
    }

    const userId = user._id

    const jwtToken = jwt.sign({
        username : username,
        userId : userId,
        role : "admin"
    },JWT_SECRET)

    return res.status(200).json({
        "message" : "Admin Logged in succesfully",
        token : jwtToken
    })

})

//Route for Admin Signup, returns a JWT token
router.post("/signup", async (req, res) => {
    const username = req.body.username;
    const name = req.body.name;
    const email = req.body.email;
    const password = req.body.password;
    const phoneNumber = req.body.phoneNumber;
    const success = signupBody.safeParse(req.body)
    console.log(success)
    if(!(success.success)){
        return res.status(411).json({
            "message" : "Invalid Inputs"
        })
    }
    const existingUser = await Admin.findOne({
        username: username
    })
    
    if(existingUser){
        return res.status(411).json({
            "message" : "Username already taken"
        })
    }

    const user = await Admin.create({
        username ,
        name,
        email,
        password,
        phoneNumber
    })
   
    const userId = user._id

    const jwtToken = jwt.sign({
        username : username,
        userId : userId,
        role : "admin"
    },JWT_SECRET)

    return res.status(200).json({
        "message" : "Admin created succesfully",
        token : jwtToken
    })
});

//Route to get all books from the database
router.get("/books",adminAuthMiddleware, async (req, res) => {
    const books = await Books.find({})
    return res.status(200).json(books)
})

//Route to create a new book in the database
router.post("/books",async (req, res) => {
    const name = req.body.name;
    const author = req.body.author;
    const book = await Books.create({
        name,
        author,
        available : true
    })
    if(!book){
        return res.status(411).json({
            "message" : "Book creation failed"
        })
    }
    return res.status(200).json({
        "message" : "Book created successfully"
    })
})

//Route to delete a book from the database
router.delete("/books/:id",async (req, res) => {
    const bookId = req.params.id;
    const book = await Books.findOne({
        _id : bookId
    })
    if(!book){
        return res.status(411).json({
            "message" : "Book not found"
        })
    }
    await Books.deleteOne({
        _id : bookId
    })
    return res.status(200).json({
        "message" : "Book deleted successfully"
    })
})

//Route to get all transactions of a user
router.get("/transactions/:userId", async (req, res) => {
    const userId = req.params.userId;

    try {
        const transactions = await Transactions.find({ userId });
        return res.status(200).json(transactions);
    } catch (error) {
        console.error(error);
        return res.status(500).json({ "message": "Internal server error" });
    }
});


//Route to issue a book to a user
router.post("/books/:id", async (req, res) => {
    const bookId = req.params.id;
    const userId = req.body.userId;

    try {
        const availableBook = await Books.findById(bookId);
        
        if (!availableBook) {
            return res.status(404).json({ "message": "Book not found" });
        }

        if (!availableBook.available) {
            return res.status(411).json({ "message": "Book not available" });
        }

        const book = await Books.findByIdAndUpdate(bookId, { available: false }, { new: true });
        
        if (!book) {
            return res.status(500).json({ "message": "Failed to update book status" });
        }

        const dueDate = new Date();
        dueDate.setDate(dueDate.getDate() + 14); // Assuming a 2-week due date

        const transaction = await Transactions.create({
            bookId,
            userId,
            dueDate,
            status: "Issued"
        });

        return res.status(200).json({ "message": "Book Issued", transaction });
    } catch (error) {
        console.error(error);
        return res.status(500).json({ "message": "Internal server error" });
    }
});

//Route to get all users
router.get("/users", async (req, res) => {
    const users = await User.find({})
    return res.status(200).json(users)
})

//Route to return a book issues to a user
router.post("/return/:id", async (req, res) => { 
    const bookId = req.params.id;
    const book = await Books.findOne({
        _id : bookId
    })
    if(!book){
        return res.status(411).json({
            "message" : "Book not found"
        })
    }
    const transaction = await Transactions.findOne({
        bookId : bookId,
    })
    if(!transaction){
        return res.status(411).json({
            "message" : "Transaction not found"
        })
    }
    await Transactions.deleteOne({
        bookId : bookId,
    })
    await Books.updateOne({
        _id : bookId
    },{
        available : true
    })
    return res.status(200).json({
        "message" : "Book returned successfully"
    })
})

export default router;