import express from 'express';
import zod from 'zod';
import jwt from 'jsonwebtoken';
import { User, Books, Transactions } from '../db';

const router = express.Router();
const JWT_SECRET = process.env.JWT_SECRET || 'Codefeast';

const signupBody = zod.object({
    username : zod.string(),
    name : zod.string(),
    email : zod.string().email(),
    password : zod.string(),
    phoneNumber : zod.string()
})

const loginBody= zod.object({
    username : zod.string(),
    password : zod.string()
})

router.post("/login", async (req, res) => {
    const username = req.body.username;
    const password = req.body.password;

    const success = loginBody.safeParse(req.body)
    if(!(success.success)){
        return res.status(411).json({
            "message" : "Invalid Inputs"
        })
    }

    const user= await User.findOne({
        username : username,
        password : password
    })
    console.log(user)

    if(!user){
        return res.status(411).json({
            "message" : "Invalid Credentials"
        })
    }

    const userId = user._id

    const jwtToken = jwt.sign({
        username : username,
        userId : userId,
        role : "user"
    },JWT_SECRET)

    return res.status(200).json({
        "message" : "User Logged In succesfully",
        token : jwtToken
    })

})

router.post("/signup", async (req, res) => {
    const username = req.body.username;
    const name = req.body.name;
    const email = req.body.email;
    const password = req.body.password;
    const phoneNumber = req.body.phoneNumber;
    const success = signupBody.safeParse(req.body)
    console.log(success)
    if(!(success.success)){
        return res.status(411).json({
            "message" : "Invalid Inputs"
        })
    }
    const existingUser = await User.findOne({
        username: username
    })
    
    if(existingUser){
        return res.status(411).json({
            "message" : "Username already taken"
        })
    }

    const user = await User.create({
        username ,
        name,
        email,
        password,
        phoneNumber
    })
   
    const userId = user._id

    const jwtToken = jwt.sign({
        username : username,
        userId : userId,
        role : "user"
    },JWT_SECRET)

    return res.status(200).json({
        "message" : "User created succesfully",
        token : jwtToken
    })
});

router.get("/books", async (req, res) => {
    const books = await Books.find({})
    return res.status(200).json(books)
})

router.get("/transactions/:userId", async (req, res) => {
    const userId = req.params.userId;

    try {
        const transactions = await Transactions.find({ userId });
        return res.status(200).json(transactions);
    } catch (error) {
        console.error(error);
        return res.status(500).json({ "message": "Internal server error" });
    }
});

    
export default router;