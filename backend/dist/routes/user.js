"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const zod_1 = __importDefault(require("zod"));
const jsonwebtoken_1 = __importDefault(require("jsonwebtoken"));
const db_1 = require("../db");
const router = express_1.default.Router();
const JWT_SECRET = process.env.JWT_SECRET || 'Codefeast';
const signupBody = zod_1.default.object({
    username: zod_1.default.string(),
    name: zod_1.default.string(),
    email: zod_1.default.string().email(),
    password: zod_1.default.string(),
    phoneNumber: zod_1.default.string()
});
const loginBody = zod_1.default.object({
    username: zod_1.default.string(),
    password: zod_1.default.string()
});
router.post("/login", (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const username = req.body.username;
    const password = req.body.password;
    const success = loginBody.safeParse(req.body);
    if (!(success.success)) {
        return res.status(411).json({
            "message": "Invalid Inputs"
        });
    }
    const user = yield db_1.User.findOne({
        username: username,
        password: password
    });
    console.log(user);
    if (!user) {
        return res.status(411).json({
            "message": "Invalid Credentials"
        });
    }
    const userId = user._id;
    const jwtToken = jsonwebtoken_1.default.sign({
        username: username,
        userId: userId,
        role: "user"
    }, JWT_SECRET);
    return res.status(200).json({
        "message": "User Logged In succesfully",
        token: jwtToken
    });
}));
router.post("/signup", (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const username = req.body.username;
    const name = req.body.name;
    const email = req.body.email;
    const password = req.body.password;
    const phoneNumber = req.body.phoneNumber;
    const success = signupBody.safeParse(req.body);
    console.log(success);
    if (!(success.success)) {
        return res.status(411).json({
            "message": "Invalid Inputs"
        });
    }
    const existingUser = yield db_1.User.findOne({
        username: username
    });
    if (existingUser) {
        return res.status(411).json({
            "message": "Username already taken"
        });
    }
    const user = yield db_1.User.create({
        username,
        name,
        email,
        password,
        phoneNumber
    });
    const userId = user._id;
    const jwtToken = jsonwebtoken_1.default.sign({
        username: username,
        userId: userId,
        role: "user"
    }, JWT_SECRET);
    return res.status(200).json({
        "message": "User created succesfully",
        token: jwtToken
    });
}));
router.get("/books", (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const books = yield db_1.Books.find({});
    return res.status(200).json(books);
}));
router.get("/transactions/:userId", (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const userId = req.params.userId;
    try {
        const transactions = yield db_1.Transactions.find({ userId });
        return res.status(200).json(transactions);
    }
    catch (error) {
        console.error(error);
        return res.status(500).json({ "message": "Internal server error" });
    }
}));
exports.default = router;
