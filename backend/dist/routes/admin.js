"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const zod_1 = __importDefault(require("zod"));
const jsonwebtoken_1 = __importDefault(require("jsonwebtoken"));
const db_1 = require("../db");
const adminAuthMiddleware_1 = __importDefault(require("../middleware/adminAuthMiddleware"));
const router = express_1.default.Router();
const JWT_SECRET = process.env.JWT_SECRET || 'Codefeast';
const signupBody = zod_1.default.object({
    username: zod_1.default.string(),
    name: zod_1.default.string(),
    email: zod_1.default.string().email(),
    password: zod_1.default.string(),
    phoneNumber: zod_1.default.string()
});
const loginBody = zod_1.default.object({
    username: zod_1.default.string(),
    password: zod_1.default.string()
});
//Route for Admin Login, returns a JWT token
router.post("/login", (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const username = req.body.username;
    const password = req.body.password;
    const success = loginBody.safeParse(req.body);
    if (!(success.success)) {
        return res.status(411).json({
            "message": "Invalid Inputs"
        });
    }
    const user = yield db_1.Admin.findOne({
        username: username,
        password: password
    });
    if (!user) {
        return res.status(411).json({
            "message": "Invalid Credentials"
        });
    }
    const userId = user._id;
    const jwtToken = jsonwebtoken_1.default.sign({
        username: username,
        userId: userId,
        role: "admin"
    }, JWT_SECRET);
    return res.status(200).json({
        "message": "Admin Logged in succesfully",
        token: jwtToken
    });
}));
//Route for Admin Signup, returns a JWT token
router.post("/signup", (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const username = req.body.username;
    const name = req.body.name;
    const email = req.body.email;
    const password = req.body.password;
    const phoneNumber = req.body.phoneNumber;
    const success = signupBody.safeParse(req.body);
    console.log(success);
    if (!(success.success)) {
        return res.status(411).json({
            "message": "Invalid Inputs"
        });
    }
    const existingUser = yield db_1.Admin.findOne({
        username: username
    });
    if (existingUser) {
        return res.status(411).json({
            "message": "Username already taken"
        });
    }
    const user = yield db_1.Admin.create({
        username,
        name,
        email,
        password,
        phoneNumber
    });
    const userId = user._id;
    const jwtToken = jsonwebtoken_1.default.sign({
        username: username,
        userId: userId,
        role: "admin"
    }, JWT_SECRET);
    return res.status(200).json({
        "message": "Admin created succesfully",
        token: jwtToken
    });
}));
//Route to get all books from the database
router.get("/books", adminAuthMiddleware_1.default, (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const books = yield db_1.Books.find({});
    return res.status(200).json(books);
}));
//Route to create a new book in the database
router.post("/books", (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const name = req.body.name;
    const author = req.body.author;
    const book = yield db_1.Books.create({
        name,
        author,
        available: true
    });
    if (!book) {
        return res.status(411).json({
            "message": "Book creation failed"
        });
    }
    return res.status(200).json({
        "message": "Book created successfully"
    });
}));
//Route to delete a book from the database
router.delete("/books/:id", (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const bookId = req.params.id;
    const book = yield db_1.Books.findOne({
        _id: bookId
    });
    if (!book) {
        return res.status(411).json({
            "message": "Book not found"
        });
    }
    yield db_1.Books.deleteOne({
        _id: bookId
    });
    return res.status(200).json({
        "message": "Book deleted successfully"
    });
}));
//Route to get all transactions of a user
router.get("/transactions/:userId", (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const userId = req.params.userId;
    try {
        const transactions = yield db_1.Transactions.find({ userId });
        return res.status(200).json(transactions);
    }
    catch (error) {
        console.error(error);
        return res.status(500).json({ "message": "Internal server error" });
    }
}));
//Route to issue a book to a user
router.post("/books/:id", (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const bookId = req.params.id;
    const userId = req.body.userId;
    try {
        const availableBook = yield db_1.Books.findById(bookId);
        if (!availableBook) {
            return res.status(404).json({ "message": "Book not found" });
        }
        if (!availableBook.available) {
            return res.status(411).json({ "message": "Book not available" });
        }
        const book = yield db_1.Books.findByIdAndUpdate(bookId, { available: false }, { new: true });
        if (!book) {
            return res.status(500).json({ "message": "Failed to update book status" });
        }
        const dueDate = new Date();
        dueDate.setDate(dueDate.getDate() + 14); // Assuming a 2-week due date
        const transaction = yield db_1.Transactions.create({
            bookId,
            userId,
            dueDate,
            status: "Issued"
        });
        return res.status(200).json({ "message": "Book Issued", transaction });
    }
    catch (error) {
        console.error(error);
        return res.status(500).json({ "message": "Internal server error" });
    }
}));
//Route to get all users
router.get("/users", (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const users = yield db_1.User.find({});
    return res.status(200).json(users);
}));
//Route to return a book issues to a user
router.post("/return/:id", (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const bookId = req.params.id;
    const book = yield db_1.Books.findOne({
        _id: bookId
    });
    if (!book) {
        return res.status(411).json({
            "message": "Book not found"
        });
    }
    const transaction = yield db_1.Transactions.findOne({
        bookId: bookId,
    });
    if (!transaction) {
        return res.status(411).json({
            "message": "Transaction not found"
        });
    }
    yield db_1.Transactions.deleteOne({
        bookId: bookId,
    });
    yield db_1.Books.updateOne({
        _id: bookId
    }, {
        available: true
    });
    return res.status(200).json({
        "message": "Book returned successfully"
    });
}));
exports.default = router;
