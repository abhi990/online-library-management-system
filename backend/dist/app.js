"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express = require('express');
const cors = require('cors');
const index_1 = __importDefault(require("./routes/index"));
const dotenv_1 = __importDefault(require("dotenv"));
dotenv_1.default.config();
const app = express();
// Middleware
app.use(express.json());
app.use(cors());
app.use("/api", index_1.default);
app.get("/", (req, res) => {
    res.status(200).json({ message: "Server is working" });
});
const PORT = process.env.PORT || 3000; // Use environment variable or default to 3000
app.listen(PORT, () => { console.log(`Server started at http://localhost:${PORT}`); });
