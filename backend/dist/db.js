"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Transactions = exports.Books = exports.Admin = exports.User = void 0;
const mongoose_1 = __importDefault(require("mongoose"));
const dotenv_1 = __importDefault(require("dotenv"));
dotenv_1.default.config();
const DATABASE_URL = process.env.DATABASE_URL || 'mongodb://localhost:27017/';
mongoose_1.default.connect(DATABASE_URL)
    .then(() => {
    console.log('Database connected');
})
    .catch((err) => {
    console.error('Database connection error:', err);
});
const userSchema = new mongoose_1.default.Schema({
    name: String,
    username: String,
    password: String,
    email: String,
    phoneNumber: String,
    books: [{ type: mongoose_1.default.Schema.Types.ObjectId, ref: 'Books' }],
});
const User = mongoose_1.default.model('User', userSchema);
exports.User = User;
const adminSchema = new mongoose_1.default.Schema({
    name: String,
    username: String,
    password: String,
    email: String,
    phoneNumber: String,
});
const Admin = mongoose_1.default.model('Admin', adminSchema);
exports.Admin = Admin;
const bookSchema = new mongoose_1.default.Schema({
    name: { type: String, minLength: 1 },
    author: { type: String, minLength: 1 },
    available: { type: Boolean, default: false },
});
const Books = mongoose_1.default.model('Books', bookSchema);
exports.Books = Books;
const transactionSchema = new mongoose_1.default.Schema({
    bookId: { type: mongoose_1.default.Schema.Types.ObjectId, ref: 'Books' },
    userId: { type: mongoose_1.default.Schema.Types.ObjectId, ref: 'User' },
    dueDate: Date,
    status: String,
});
const Transactions = mongoose_1.default.model('Transactions', transactionSchema);
exports.Transactions = Transactions;
