"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const jwt = require("jsonwebtoken");
const JWT_SECRET = "Codefeast";
function adminAuthMiddleware(req, res, next) {
    const authorization = req.headers.authorization;
    try {
        const isValid = jwt.verify(authorization, JWT_SECRET);
        req.userId = isValid.userId || isValid.userID;
        if (isValid.role !== "admin") {
            return res.status(403).json({
                "message": "User not authorized"
            });
        }
        next();
    }
    catch (err) {
        return res.status(403).json({
            "message": "User not found"
        });
    }
}
exports.default = adminAuthMiddleware;
