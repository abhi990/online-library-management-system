const express = require('express');
const cors = require('cors');
import rootRouter from './routes/index'
import dotenv from 'dotenv';
dotenv.config();
const app = express();

// Middleware
app.use(express.json());
app.use(cors());

app.use("/api", rootRouter);

app.get("/" , (req, res) => {
    res.status(200).json({ message: "Server is working" });
})

const PORT = process.env.PORT || 3000; // Use environment variable or default to 3000
app.listen(PORT,()=>{console.log(`Server started at http://localhost:${PORT}`)})
