import {BrowserRouter , Routes, Route} from 'react-router-dom'
import SignIn from './pages/user/SignIn'
import SignUp from './pages/user/SignUp'
import AdminSignIn from './pages/admin/AdminSignIn'
import AdminSignUp from './pages/admin/AdminSignUp'
import AdminDashboard from './pages/admin/AdminDashboard'
import UserDashboard from './pages/user/UserDashboard'
import Users from './components/Users'

function App() {

  return (
    <BrowserRouter>
      <Routes>
        <Route path="/signin" element={<SignIn />} />
        <Route path="/signup" element={<SignUp />} />
        <Route path="/adminlogin" element={<AdminSignIn />} />
        <Route path="/adminsignup" element={<AdminSignUp />} />
        <Route path='/admindashboard' element={<AdminDashboard />} />
        <Route path='/userdashboard' element={<UserDashboard />} />
        <Route path='/*' element={<SignIn />} />
        <Route path='/users' element={<Users />} />
      </Routes>
    </BrowserRouter>
  )
}

export default App