import { useEffect,useState } from 'react'
import axios from 'axios'

type propsType={
    bookAdded : boolean
}

const Books: React.FC<propsType> = ({bookAdded} )=>{
    const [books, setBooks] = useState([])
    const [userId,setUserId] = useState("")
    const [bookChanged,setBookChanged] = useState(false)

    interface Book {
        _id: string;
        name: string;
        author : string;
        available : boolean;
    }

    useEffect(()=>{
        const fetchBooks = async ()=>{
            const response =await axios.get("https://librarybackend-1-ypw9.onrender.com/api/user/books/")
            console.log(response.data)
            setBooks(response.data)
        }
        fetchBooks()
    },[bookChanged,bookAdded])

  return (
    <div>
        <div className='px-16'>
                <div className="text-white text-center font-extrabold text-[2rem]">
                    Available Books
                </div>
                <div className='flex items-center justify-center'>
                    <div className="flex flex-wrap gap-4 ">
                        {books.map((book : Book) => (
                            <div key={book._id} className="flex flex-col items-center justify-center bg-gray-200 rounded-md p-4 w-64">
                                <p className="font-bold text-center">Book Name: {book.name}</p>
                                <p className='mb-2'>Author: {book.author}</p>
                                {book.available ?
                                <div className='flex flex-col justify-center items-center'> 
                                    <input type='text' onChange={e=>{setUserId(e.target.value)}} placeholder='Enter user id' className='border border-black rounded p-2'/> 
                                    <div className='flex gap-4'>
                                    <button onClick={async ()=>{
                                                console.log(book._id)
                                                await axios.post(`https://librarybackend-1-ypw9.onrender.com/api/admin/books/${book._id}`,{
                                                    userId : userId
                                                }).then((response)=>{
                                                    console.log(response.data)
                                                    setBookChanged(!bookChanged)
                                                })
                                                //window.location.reload();
                                            }} className='mt-4 border  bg-black text-white font-semibold border-black rounded p-2'>
                                        Issue
                                    </button>
                                    <button onClick={async ()=>{
                                                console.log(book._id)
                                                await axios.delete(`https://librarybackend-1-ypw9.onrender.com/api/admin/books/${book._id}`).then((response)=>{
                                                    console.log(response.data)
                                                    setBookChanged(!bookChanged)
                                                })
                                                //window.location.reload();
                                            }} className='mt-4 border  bg-black text-white font-semibold border-black rounded p-2'>
                                        Delete
                                    </button>
                                    </div>
                                </div>
                                
                                : <div className='flex gap-4'>
                                    <button onClick={async ()=>{
                                            console.log(book._id)
                                            await axios.post(`https://librarybackend-1-ypw9.onrender.com/api/admin/return/${book._id}`,{
                                                userId : userId
                                            })
                                            //window.location.reload();
                                            setBookChanged(!bookChanged)
                                        }} className='mt-4 border bg-black text-white font-semibold border-black rounded p-2'>
                                    Return</button> 
                                    <button onClick={async ()=>{
                                                console.log(book._id)
                                                await axios.delete(`https://librarybackend-1-ypw9.onrender.com/api/admin/books/${book._id}`)
                                                //window.location.reload();
                                                setBookChanged(!bookChanged)
                                            }} className='mt-4 border  bg-black text-white font-semibold border-black rounded p-2'>
                                        Delete
                                    </button>
                                </div>}
                                
                            </div>
                        ))}
                    </div>
                </div>
        </div>

    </div>
  )
}

export default Books