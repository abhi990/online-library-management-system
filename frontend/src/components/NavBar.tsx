import { useEffect, useState } from 'react';
import {jwtDecode} from 'jwt-decode';
import { Link, useNavigate } from 'react-router-dom';

type Role = {
  username: string,
  role: string
}



const NavBar = () => {
  const [role, setRole] = useState<Role | null>(null);
  const navigate = useNavigate();

  useEffect(() => {
    const token = localStorage.getItem('token');
    if (token) {
      const decoded: Role = jwtDecode(token);
      setRole(decoded);
    }
  }, []);

  return (
    <div className='bg-gray-400 flex border-b p-4 justify-between items-center'>
      <Link to="/">
        <h1 className='text-[2rem] font-extrabold'>Library</h1>
      </Link>
      <div className='flex flex-col items-center'>
        <div className='flex items-center'>
          {role && (
            <>
              <h1 className='mr-4 font-semibold sm:text-[1.5rem]'>{role.username}</h1>
              <div className="font-extrabold sm:text-[2rem] rounded-full bg-gray-500 p-2 inline-flex items-center justify-center">
                {role.role === 'admin' ? 'A' : 'U'}
              </div>
            </>
          )}
        </div>
        <div>
          <button
            onClick={() => {
              localStorage.removeItem('token');
              navigate('/login');
            }}
            className='bg-black text-white sm:p-2 text-[0.75rem] p-2 rounded-md'
          >Log Out </button>
          </div>
      </div>
    </div>
  );
}

export default NavBar;
