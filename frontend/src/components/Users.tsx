import { useEffect,useState } from "react"
import axios from "axios";

const Users = () => {
    const [users, setUsers] = useState([]);

    useEffect(() => {
        const fetchUsers = async () => {
            try {
                const response = await axios.get(`https://librarybackend-1-ypw9.onrender.com/api/admin/users`);
                console.log(response.data);
                setUsers(response.data);
            } catch (error) {
                console.error('Error fetching users:', error);
            }
        };

        fetchUsers();
    }, []);
  return (
    <div>
        <div className="text-white text-center font-extrabold text-[2rem]">
            List Of Users
        </div>
        <ul className=" flex flex-col items-center rounded-md">
            {users.map((user: any) => (
                <li key={user.id} className="bg-white rounded p-2 m-2 mb-4 flex flex-col sm:flex-row justify-between items-center">
                    <span className="font-semibold text-gray-800 mr-4">Username: {user.username}</span>
                    <span className="text-gray-600">UserId: {user._id}</span>
                </li>
            ))}
        </ul>

    </div>
  )
}

export default Users