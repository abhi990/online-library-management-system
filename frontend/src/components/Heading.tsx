type HeadingProps = {
  text: string
}

const Heading = (props :  HeadingProps ) => {
  return (
    <div className='text-[3rem] font-bold text-center py-4'>
        {props.text}
    </div>
  )
}

export default Heading