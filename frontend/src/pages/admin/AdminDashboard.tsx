import Heading from '../../components/Heading'
import NavBar from "../../components/NavBar"
import Users from '../../components/Users'
import {useState,useEffect} from 'react'
import axios from 'axios'

const AdminDashboard = () => {
  const [showModal, setShowModal] = useState(false);
  const [name, setName] = useState('');
  const [author, setAuthor] = useState('');
  const [bookAdded,setBookAdded] = useState(false)

  const [books, setBooks] = useState([])
  const [userId,setUserId] = useState("")
  const [bookChanged,setBookChanged] = useState(false)

    interface Book {
        _id: string;
        name: string;
        author : string;
        available : boolean;
    }

    useEffect(()=>{
        const fetchBooks = async ()=>{
            const response =await axios.get("https://librarybackend-1-ypw9.onrender.com/api/user/books/")
            console.log(response.data)
            setBooks(response.data)
        }
        fetchBooks()
    },[bookChanged,bookAdded])

  return (
    <div>
      <NavBar />
      <h1><Heading text="Admin Dashboard" /></h1>
      <div className='flex justify-center'>
        <button onClick={()=>{setShowModal(true)}} className='m-4 p-6 rounded-lg border bg-black text-white text-[1.5rem]  border-black font-extrabold '>Add New Book</button>
      </div>
      {/* <Books bookAdded={bookAdded}/> */}
      <div>
        <div className='px-16'>
                <div className="text-white text-center font-extrabold text-[2rem]">
                    Available Books
                </div>
                <div className='flex items-center justify-center'>
                    <div className="flex flex-wrap gap-4 ">
                        {books.map((book : Book) => (
                            <div key={book._id} className="flex flex-col items-center justify-center bg-gray-200 rounded-md p-4 w-64">
                                <p className="font-bold text-center">Book Name: {book.name}</p>
                                <p className='mb-2'>Author: {book.author}</p>
                                {book.available ?
                                <div className='flex flex-col justify-center items-center'> 
                                    <input type='text' onChange={e=>{setUserId(e.target.value)}} placeholder='Enter user id' className='border border-black rounded p-2'/> 
                                    <div className='flex gap-4'>
                                    <button onClick={async ()=>{
                                                console.log(book._id)
                                                await axios.post(`https://librarybackend-1-ypw9.onrender.com/api/admin/books/${book._id}`,{
                                                    userId : userId
                                                }).then((response)=>{
                                                    console.log(response.data)
                                                    setBookChanged(!bookChanged)
                                                })
                                                //window.location.reload();
                                            }} className='mt-4 border  bg-black text-white font-semibold border-black rounded p-2'>
                                        Issue
                                    </button>
                                    <button onClick={async ()=>{
                                                console.log(book._id)
                                                await axios.delete(`https://librarybackend-1-ypw9.onrender.com/api/admin/books/${book._id}`).then((response)=>{
                                                    console.log(response.data)
                                                    setBookChanged(!bookChanged)
                                                })
                                                //window.location.reload();
                                            }} className='mt-4 border  bg-black text-white font-semibold border-black rounded p-2'>
                                        Delete
                                    </button>
                                    </div>
                                </div>
                                
                                : <div className='flex gap-4'>
                                    <button onClick={async ()=>{
                                            console.log(book._id)
                                            await axios.post(`https://librarybackend-1-ypw9.onrender.com/api/admin/return/${book._id}`,{
                                                userId : userId
                                            })
                                            //window.location.reload();
                                            setBookChanged(!bookChanged)
                                        }} className='mt-4 border bg-black text-white font-semibold border-black rounded p-2'>
                                    Return</button> 
                                    <button onClick={async ()=>{
                                                console.log(book._id)
                                                await axios.delete(`https://librarybackend-1-ypw9.onrender.com/api/admin/books/${book._id}`)
                                                //window.location.reload();
                                                setBookChanged(!bookChanged)
                                            }} className='mt-4 border  bg-black text-white font-semibold border-black rounded p-2'>
                                        Delete
                                    </button>
                                </div>}
                                
                            </div>
                        ))}
                    </div>
                </div>
        </div>

    </div>
      <Users />
      {showModal && (
        <div className="fixed inset-0 flex items-center justify-center z-50">
          <div className="bg-white p-8 rounded-lg shadow-lg">
            <h2 className="text-2xl font-bold mb-4">Add New Book</h2>
            <div className="mb-4">
              <label htmlFor="name" className="block mb-2">Name:</label>
              <input type="text" id="name" value={name} onChange={(e) => setName(e.target.value)} className="border border-gray-300 rounded-lg p-2" />
            </div>
            <div className="mb-4">
              <label htmlFor="author" className="block mb-2">Author:</label>
              <input type="text" id="author" value={author} onChange={(e) => setAuthor(e.target.value)} className="border border-gray-300 rounded-lg p-2" />
            </div>
            <div className="flex justify-end">
              <button className="bg-black text-white px-4 py-2 rounded-lg" onClick={async ()=>{
                      await axios.post('https://librarybackend-1-ypw9.onrender.com/api/admin/books', {
                        name,
                        author
                      });
                      setBookAdded(!bookAdded)
                      setShowModal(false);}}>
              Add Book
              </button>
              <button className="bg-gray-300 text-gray-700 px-4 py-2 rounded-lg ml-2" onClick={() => setShowModal(false)}>Cancel</button>
            </div>
          </div>
        </div>
      )}
    </div>

  )
}

export default AdminDashboard