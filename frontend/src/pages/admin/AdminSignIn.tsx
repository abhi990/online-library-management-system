import { useState } from 'react'
import Heading from '../../components/Heading'
import axios from 'axios'
import { useNavigate,Link } from 'react-router-dom'

const AdminSignIn = () => {
  const [username,setUsername] = useState("")
  const [password,setPassword] =useState("")
  const navigate =useNavigate()
  return (
    <div className='flex flex-col h-[100vh] items-center justify-center'>
        <div className='bg-white flex flex-col sm:px-16 p-8  rounded-md shadow '>   
            <Heading text={"Admin Sign In"} />
            <p className='text-gray-500 font-[1.5rem] '>Enter your credentials to access your account</p>
            <label className='mt-2 mb-2 text-[1.5rem] font-semibold'>Username</label>
            <input onChange={(e)=>{setUsername(e.target.value)}} className='border border-gray-500 rounded-md p-2' type='text' placeholder='Your Username here!' />
            <label className='mt-2 mb-2 text-[1.5rem] font-semibold'>Password</label>
            <input onChange={(e)=>{setPassword(e.target.value)}} className='border border-gray-500 rounded-md p-2' type='text' placeholder='Your Password here!' />
            <button onClick={
                async ()=>{
                    const response =await axios.post("https://librarybackend-1-ypw9.onrender.com/api/admin/login",{
                        username,
                        password
                    })
                    console.log(response.data)
                    if(await response.status === 411){
                        alert("Invalid Credentials")
                    }
                    else{
                        localStorage.setItem("token",response.data.token)
                        navigate('/admindashboard')
                    }
                }
            } className='border py-2 mt-6 bg-black text-white rounded-md font-bold mx-8'>Sign In</button>
            <p className='text-gray-500 font-[1.5rem] mt-4'>Don't have an account? <Link to="/adminsignup">Sign Up</Link></p>
        </div>       
    </div>
  )
}

export default AdminSignIn