import { useState } from 'react'
import Heading from '../../components/Heading'
import axios from 'axios'
import {Link, useNavigate} from 'react-router-dom'


const SignUp = () => {
  const [username,setUsername] = useState("")
  const [password,setPassword] = useState("")
  const [email,setEmail] = useState("")
  const [name,setName] = useState("")
  const [phoneNumber,setPhoneNumber] = useState("")
  const navigate = useNavigate()


  return (
    <div className='flex flex-col h-[100vh] items-center justify-center'>
        <div className='bg-white sm:px-8 flex flex-col border rounded-md shadow'>   
            <Heading text={"User Sign Up"} />
            <p className='text-gray-500 font-[1.5rem] mb-4'>Enter your credentials to create a new account</p>
            <label className='  text-[1.5rem] font-semibold'>Username</label>
            <input onChange={(e)=>{setUsername(e.target.value)}} className='border border-gray-500 rounded-md p-2' type='text' placeholder='Your Username Here!' />
            <label className='  text-[1.5rem] font-semibold'>Name</label>
            <input onChange={(e)=>{setName(e.target.value)}} className='border border-gray-500 rounded-md p-2' type='text' placeholder='Your Name Here!' />
            <label className='  text-[1.5rem] font-semibold'>Password</label>
            <input onChange={(e)=>{setPassword(e.target.value)}} className='border border-gray-500 rounded-md p-2' type='text' placeholder='Your Password here!' />
            <label className='  text-[1.5rem] font-semibold'>Email</label>
            <input onChange={(e)=>{setEmail(e.target.value)}} className='border border-gray-500 rounded-md p-2' type='text' placeholder='example@gmail.com' />
            <label className='  text-[1.5rem] font-semibold'>Phone Number</label>
            <input onChange={(e)=>{setPhoneNumber(e.target.value)}} className='border border-gray-500 rounded-md p-2' type='text' placeholder='+91 XXXXXXXXXX' />
            <button onClick={async ()=>{
              const response = await axios.post("https://librarybackend-1-ypw9.onrender.com/api/user/signup",{
                name,
                username,
                password,
                email,
                phoneNumber
              }
            )
            localStorage.setItem("token",response.data.token)
            navigate("/userdashboard")
            }} className='border p-2 bg-gray-700 hover:bg-gray-800 text-white rounded-md font-bold m-4'>Sign Up</button>
            <p className='text-center mb-2 text-gray-500 font-[1.5rem]'>Already have an account? <Link className='underline' to="signin">Sign In Here!</Link></p>
        </div>       
    </div>
  )
}

export default SignUp