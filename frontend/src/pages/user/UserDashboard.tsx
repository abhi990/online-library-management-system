import { useEffect, useState } from "react";
import Heading from '../../components/Heading';
import NavBar from "../../components/NavBar";
import axios from 'axios';
import {jwtDecode} from 'jwt-decode'; // Correct import statement

const UserDashboard = () => {
    const [userId, setUserId] = useState('');
    const [transactions, setTransactions] = useState([]);
    const [loading, setLoading] = useState(true); // Loading state

    useEffect(() => {
        const token = localStorage.getItem('token');
        if (token) {
            const decoded = jwtDecode<{ userId: string }>(token);
            console.log(decoded.userId);
            setUserId(decoded.userId);
        }

        const fetchTransactions = async () => {
            try {
                const response = await axios.get(`https://librarybackend-1-ypw9.onrender.com/api/user/transactions/${userId}`);
                console.log(response.data);
                setTransactions(response.data);
                setLoading(false); // Set loading state to false when transactions are fetched
            } catch (error) {
                console.error('Error fetching transactions:', error);
                setLoading(false); // Set loading state to false on error
            }
        };

        if (userId) {
            fetchTransactions();
        }
    }, [userId]);

    return (
        <div>
            <NavBar />
            <div className="text-white">
                <Heading text={"User Dashboard"} />
                {loading ? (
                    <p className="text-center">Loading...</p>
                ) : (
                    <div className="flex flex-col justify-center items-center">
                        {transactions.map((transaction: any) => (
                            <div className="bg-white text-black m-4 p-4 rounded-md" key={transaction.id}>
                                <p>Book Id: {transaction.bookId}</p>
                                <p>Transaction Date: {transaction.dueDate.slice(0,10)}</p>
                                <p>Status: {transaction.status}</p>
                            </div>
                        ))}
                    </div>
                )}
            </div>
        </div>
    );
}

export default UserDashboard;
